/*! \file linearSolverNicslu.h
 */

#include "omc_config.h"

#ifdef WITH_UMFPACK
#ifndef _LINEARSOLVERNICSLU_H_
#define _LINEARSOLVERNICSLU_H_

#include "simulation_data.h"
//#include "suitesparse/Include/amd.h"
#include "nicslu/include/nicslu.h"

typedef struct DATA_NICSLU
{
  uint__t *Ap;
  uint__t *Ai;
  real__t *Ax;
  uint__t n_col;
  uint__t n_row;
  uint__t nnz;
  SNicsLU* nicslu;

  double* work;

  rtclock_t timeClock;             /* time clock */
  int numberSolving;
  int numberOfThreads;              // self-explainatory

} DATA_NICSLU;

int allocateNicsluData(int n_row, int n_col, int nz, void **data);
int freeNicsluData(void **data);
int solveNicslu(DATA *data, threadData_t *threadData, int sysNumber);

#endif
#endif
