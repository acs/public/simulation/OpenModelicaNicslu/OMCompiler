/*! \file linearSolverNicslu.c
 */

#include "omc_config.h"

//#ifdef WITH_UMFPACK
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "model_help.h"
#include "simulation/simulation_info_json.h"
#include "simulation_data.h"
#include "util/omc_error.h"
#include "util/varinfo.h"

#include "linearSolverNicslu.h"
#include "linearSystem.h"

static void printMatrixCSC(int *Ap, int *Ai, double *Ax, int n);
static void printMatrixCSR(int *Ap, int *Ai, double *Ax, int n);

/*! \fn allocate memory for linear system solver Nicslu
 *
 */
int allocateNicsluData(int n_row, int n_col, int nz, void **voiddata) {
  DATA_NICSLU *data = (DATA_NICSLU *)malloc(sizeof(DATA_NICSLU));
  assertStreamPrint(NULL, 0 != data,
                    "Could not allocate data for linear solver Nicslu.");

  data->nicslu = NULL;
  data->n_col = n_col;
  data->n_row = n_row;
  data->nnz = nz;

  data->Ap = (uint__t *)calloc((n_row + 1), sizeof(uint__t));
  data->Ai = (uint__t *)calloc(nz, sizeof(uint__t));
  data->Ax = (real__t *)calloc(nz, sizeof(real__t));
  data->work = (double *)calloc(n_col, sizeof(double));

  data->numberSolving = 0;
  data->nicslu = (SNicsLU *)malloc(sizeof(SNicsLU));
  NicsLU_Initialize(data->nicslu);
  data->numberOfThreads = 2;
  *voiddata = (void *)data;

  return 0;
}

/*! \fn free memory for linear system solver Nicslu
 *
 */
int freeNicsluData(void **voiddata) {
  TRACE_PUSH

  DATA_NICSLU *data = (DATA_NICSLU *)*voiddata;

  free(data->Ap);
  free(data->Ai);
  free(data->Ax);
  free(data->work);

  if (data->nicslu) {
    NicsLU_Destroy(data->nicslu);
  }

  TRACE_POP
  return 0;
}

/*! \fn getAnalyticalJacobian
 *
 *  function calculates analytical jacobian
 *
 *  \param [ref] [data]
 *  \param [in]  [sysNumber]
 *
 *  \author wbraun
 *
 */
static int getAnalyticalJacobian(DATA *data, threadData_t *threadData,
                                 int sysNumber) {
  int i, ii, j, k, l;
  LINEAR_SYSTEM_DATA *systemData =
      &(((DATA *)data)->simulationInfo->linearSystemData[sysNumber]);

  const int index = systemData->jacobianIndex;
  int nth = 0;
  int nnz = data->simulationInfo->analyticJacobians[index]
                .sparsePattern.numberOfNoneZeros;

  for (i = 0; i < data->simulationInfo->analyticJacobians[index].sizeRows;
       i++) {
    data->simulationInfo->analyticJacobians[index].seedVars[i] = 1;

    ((systemData->analyticalJacobianColumn))(data, threadData);

    for (j = 0; j < data->simulationInfo->analyticJacobians[index].sizeCols;
         j++) {
      if (data->simulationInfo->analyticJacobians[index].seedVars[j] == 1) {
        ii = data->simulationInfo->analyticJacobians[index]
                 .sparsePattern.leadindex[j];
        while (ii < data->simulationInfo->analyticJacobians[index]
                        .sparsePattern.leadindex[j + 1]) {
          l = data->simulationInfo->analyticJacobians[index]
                  .sparsePattern.index[ii];
          systemData->setAElement(
              i, l,
              -data->simulationInfo->analyticJacobians[index].resultVars[l],
              nth, (void *)systemData, threadData);
          nth++;
          ii++;
        };
      }
    };

    /* de-activate seed variable for the corresponding color */
    data->simulationInfo->analyticJacobians[index].seedVars[i] = 0;
  }

  return 0;
}

/*! \fn residual_wrapper for the residual function
 *
 */
static int residual_wrapper(double *x, double *f, void **data, int sysNumber) {
  int iflag = 0;

  (*((DATA *)data[0])
        ->simulationInfo->linearSystemData[sysNumber]
        .residualFunc)(data, x, f, &iflag);
  return 0;
}

/*! \fn solve linear system with Nicslu method
 *
 *  \param  [in]  [data]
 *                [sysNumber] index of the corresponding linear system
 *
 *
 * author: wbraun
 */
int solveNicslu(DATA *data, threadData_t *threadData, int sysNumber) {
  void *dataAndThreadData[2] = {data, threadData};
  LINEAR_SYSTEM_DATA *systemData =
      &(data->simulationInfo->linearSystemData[sysNumber]);
  DATA_NICSLU *solverData = (DATA_NICSLU *)systemData->solverData[0];

  int i, j, status = 0, success = 0, n = systemData->size,
            eqSystemNumber = systemData->equationIndex,
            indexes[2] = {1, eqSystemNumber};
  double tmpJacEvalTime;
  int reuseMatrixJac =
      (data->simulationInfo->currentContext == CONTEXT_SYM_JACOBIAN &&
       data->simulationInfo->currentJacobianEval > 0);

  infoStreamPrintWithEquationIndexes(
      LOG_LS, 0, indexes,
      "Start solving Linear System %d (size %d) at time %g with Nicslu Solver",
      eqSystemNumber, (int)systemData->size, data->localData[0]->timeValue);
  SNicsLU *nicslu = solverData->nicslu;

  rt_ext_tp_tick(&(solverData->timeClock));
  if (0 == systemData->method) {
    if (!reuseMatrixJac) {
      /* set A matrix */
      solverData->Ap[0] = 0;
      systemData->setA(data, threadData, systemData);
      solverData->Ap[solverData->n_row] = solverData->nnz;
    }

    /* set b vector */
    systemData->setb(data, threadData, systemData);
  } else {

    if (!reuseMatrixJac) {
      solverData->Ap[0] = 0;
      /* calculate jacobian -> matrix A*/
      if (systemData->jacobianIndex != -1) {
        getAnalyticalJacobian(data, threadData, sysNumber);
      } else {
        assertStreamPrint(threadData, 1,
                          "jacobian function pointer is invalid");
      }
      solverData->Ap[solverData->n_row] = solverData->nnz;
    }

    /* calculate vector b (rhs) */
    memcpy(solverData->work, systemData->x, sizeof(real__t) * solverData->n_row);
    residual_wrapper(solverData->work, systemData->b, dataAndThreadData,
                     sysNumber);
  }
  tmpJacEvalTime = rt_ext_tp_tock(&(solverData->timeClock));
  systemData->jacobianTime += tmpJacEvalTime;
  infoStreamPrint(LOG_LS_V, 0, "###  %f  time to set Matrix A and vector b.",
                  tmpJacEvalTime);

  if (ACTIVE_STREAM(LOG_LS_V)) {
    infoStreamPrint(LOG_LS_V, 1, "Old solution x:");
    for (i = 0; i < solverData->n_row; ++i)
      infoStreamPrint(
          LOG_LS_V, 0, "[%d] %s = %g", i + 1,
          modelInfoGetEquation(&data->modelData->modelDataXml, eqSystemNumber)
              .vars[i],
          systemData->x[i]);
    messageClose(LOG_LS_V);

    infoStreamPrint(LOG_LS_V, 1, "Matrix A n_rows = %d", solverData->n_row);
    for (i = 0; i < solverData->n_row; i++) {
      infoStreamPrint(LOG_LS_V, 0, "%d. Ap => %d -> %d", i, solverData->Ap[i],
                      solverData->Ap[i + 1]);
      for (j = solverData->Ap[i]; j < solverData->Ap[i + 1]; j++) {
        infoStreamPrint(LOG_LS_V, 0, "A[%d,%d] = %f", i, solverData->Ai[j],
                        solverData->Ax[j]);
      }
    }
    messageClose(LOG_LS_V);

    for (i = 0; i < solverData->n_row; i++)
      infoStreamPrint(LOG_LS_V, 0, "b[%d] = %e", i, systemData->b[i]);
  }
  rt_ext_tp_tick(&(solverData->timeClock));

  int ret;
  /*NicsLU_Sort(solverData->n_col, solverData->nnz, solverData->Ax,
              solverData->Ai, solverData->Ap);*/
  /* symbolic pre-ordering of A to reduce fill-in of L and U */
  if (0 == solverData->numberSolving) {
      if (1 == systemData->method) {
        infoStreamPrint(LOG_LS, 0, "transposing matrix...");
      }
    ret = NicsLU_CreateMatrix(solverData->nicslu, solverData->n_col,
                              solverData->nnz, solverData->Ax, solverData->Ai,
                              solverData->Ap);
    if (ret != NICS_OK)
      infoStreamPrint(LOG_LS, 0, "create %d", ret);

    /*ret = NicsLU_Transpose(solverData->n_col, solverData->nnz, solverData->Ax,
                     solverData->Ai, solverData->Ap);
    if(ret != NICS_OK)
        infoStreamPrint(LOG_LS,0,"transpose %d",ret);*/

    nicslu->cfgf[0] = 1.;
    if(systemData->method==1)
        nicslu->cfgi[0] = 1;
    else
        nicslu->cfgi[0] = 0;
    ret = NicsLU_Analyze(solverData->nicslu);
    if (ret != NICS_OK)
      infoStreamPrint(LOG_LS, 0, "analyse %d", ret);
  }
  // ret = NicsLU_CreateScheduler(solverData->nicslu);
  // infoStreamPrint(LOG_LS, 0, "suggestion: %s.",
  //               ret == 0 ? "parallel" : "sequential");

  /* --------------------------------------------------------
        ATTENTION : HARDCODED THREAD INITIALIZATION AHEAD
     -------------------------------------------------------- */
  // two hardcoded threads
  // ret = NicsLU_CreateThreads(solverData->nicslu, 2, TRUE);
  // if(ret!=NICS_OK)
  //   infoStreamPrint(LOG_LS, 0, "%d", ret);
  /* --------------------------------------------------------
                    HARDCODED PART OVER
     -------------------------------------------------------- */
  // NicsLU_BindThreads(solverData->nicslu, FALSE);
  //}

  /* if reuseMatrixJac use also previous factorization */
  if (!reuseMatrixJac) {
    /* compute the LU factorization of A */
    if (NICS_OK == ret) {
      if (nicslu->flag[2] == TRUE) {
        /* Just refactor using the same pivots, but check that the refactor is
         * still accurate */
        //ret = NicsLU_ReFactorize_MT(solverData->nicslu, solverData->Ax);
        ret = NicsLU_Factorize(solverData->nicslu);
        infoStreamPrint(LOG_LS, 0, "re-factorize %d", ret);
      } else {
        //ret = NicsLU_Factorize_MT(solverData->nicslu);
        ret = NicsLU_Factorize(solverData->nicslu);
        if (ret != NICS_OK)
          infoStreamPrint(LOG_LS, 0, "factorize %d", ret);
      }
    }
  }

  ret = NicsLU_Solve(solverData->nicslu, systemData->b);
  success = NICS_OK == ret ? 1 : -1;
  infoStreamPrint(LOG_LS_V, 0, "Solve System: %f",
                  rt_ext_tp_tock(&(solverData->timeClock)));

  /* print solution */
  if (1 == success) {
    if (1 == systemData->method) {
      /* take the solution */
      for (i = 0; i < solverData->n_row; ++i) {
        systemData->x[i] += systemData->b[i];
      }
      /* update inner equations */
      residual_wrapper(systemData->x, solverData->work, dataAndThreadData,
                       sysNumber);
    } else {
      infoStreamPrint(LOG_LS, 0, "solve truncated problem");
      /* the solution is automatically in x */
      memcpy(systemData->x, systemData->b, sizeof(real__t) * systemData->size);
    }

    if (ACTIVE_STREAM(LOG_LS_V)) {
      infoStreamPrint(LOG_LS_V, 1, "Solution x:");
      infoStreamPrint(
          LOG_LS_V, 0, "System %d numVars %d.", eqSystemNumber,
          modelInfoGetEquation(&data->modelData->modelDataXml, eqSystemNumber)
              .numVar);

      for (i = 0; i < systemData->size; ++i)
        infoStreamPrint(
            LOG_LS_V, 0, "[%d] %s = %g", i + 1,
            modelInfoGetEquation(&data->modelData->modelDataXml, eqSystemNumber)
                .vars[i],
            systemData->x[i]);

      messageClose(LOG_LS_V);
    }
  } else {
    warningStreamPrint(LOG_STDOUT, 0,
                       "Failed to solve linear system of equations (no. %d) at "
                       "time %f, system status %d.",
                       (int)systemData->equationIndex,
                       data->localData[0]->timeValue, status);
  }
  solverData->numberSolving += 1;
  // NicsLU_DestroyThreads(solverData->nicslu);
  return success;
}

static void printMatrixCSC(int *Ap, int *Ai, double *Ax, int n) {
  int i, j, k, l;

  char **buffer = (char **)malloc(sizeof(char *) * n);
  for (l = 0; l < n; l++) {
    buffer[l] = (char *)malloc(sizeof(char) * n * 20);
    buffer[l][0] = 0;
  }

  k = 0;
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      if ((k < Ap[i + 1]) && (Ai[k] == j)) {
        sprintf(buffer[j], "%s %5g ", buffer[j], Ax[k]);
        k++;
      } else {
        sprintf(buffer[j], "%s %5g ", buffer[j], 0.0);
      }
    }
  }
  for (l = 0; l < n; l++) {
    infoStreamPrint(LOG_LS_V, 0, "%s", buffer[l]);
    free(buffer[l]);
  }
  free(buffer);
}

static void printMatrixCSR(int *Ap, int *Ai, double *Ax, int n) {
  int i, j, k;
  char *buffer = (char *)malloc(sizeof(char) * n * 15);
  k = 0;
  for (i = 0; i < n; i++) {
    buffer[0] = 0;
    for (j = 0; j < n; j++) {
      if ((k < Ap[i + 1]) && (Ai[k] == j)) {
        sprintf(buffer, "%s %5.2g ", buffer, Ax[k]);
        k++;
      } else {
        sprintf(buffer, "%s %5.2g ", buffer, 0.0);
      }
    }
    infoStreamPrint(LOG_LS_V, 0, "%s", buffer);
  }
  free(buffer);
}

//#endif
