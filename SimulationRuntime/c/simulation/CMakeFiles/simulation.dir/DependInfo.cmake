# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/simulation/modelinfo.c" "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/simulation/CMakeFiles/simulation.dir/modelinfo.c.o"
  "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/simulation/options.c" "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/simulation/CMakeFiles/simulation.dir/options.c.o"
  "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/simulation/simulation_info_json.c" "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/simulation/CMakeFiles/simulation.dir/simulation_info_json.c.o"
  "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/simulation/simulation_input_xml.c" "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/simulation/CMakeFiles/simulation.dir/simulation_input_xml.c.o"
  "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/simulation/simulation_omc_assert.c" "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/simulation/CMakeFiles/simulation.dir/simulation_omc_assert.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "NO_INTERACTIVE_DEPENDENCY"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/lib/expat-win32-msvc"
  "/include/lis"
  "/include/pthread"
  "OMCompiler"
  "OMCompiler/3rdParty/gc/include"
  "SimulationRuntime/c"
  "SimulationRuntime/c/linearization"
  "SimulationRuntime/c/math-support"
  "SimulationRuntime/c/meta"
  "SimulationRuntime/c/meta/gc"
  "SimulationRuntime/c/simulation"
  "SimulationRuntime/c/optimization"
  "SimulationRuntime/c/simulation/results"
  "SimulationRuntime/c/simulation/solver"
  "SimulationRuntime/c/simulation/solver/initialization"
  "SimulationRuntime/c/util"
  "OMCompiler/3rdParty/FMIL/ThirdParty/Expat/expat-2.0.1/lib"
  "OMCompiler/Compiler/runtime"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/linearization/linearize.cpp" "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/simulation/CMakeFiles/simulation.dir/__/linearization/linearize.cpp.o"
  "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/simulation/simulation_runtime.cpp" "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/simulation/CMakeFiles/simulation.dir/simulation_runtime.cpp.o"
  "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/simulation/socket.cpp" "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/simulation/CMakeFiles/simulation.dir/socket.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "NO_INTERACTIVE_DEPENDENCY"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/lib/expat-win32-msvc"
  "/include/lis"
  "/include/pthread"
  "OMCompiler"
  "OMCompiler/3rdParty/gc/include"
  "SimulationRuntime/c"
  "SimulationRuntime/c/linearization"
  "SimulationRuntime/c/math-support"
  "SimulationRuntime/c/meta"
  "SimulationRuntime/c/meta/gc"
  "SimulationRuntime/c/simulation"
  "SimulationRuntime/c/optimization"
  "SimulationRuntime/c/simulation/results"
  "SimulationRuntime/c/simulation/solver"
  "SimulationRuntime/c/simulation/solver/initialization"
  "SimulationRuntime/c/util"
  "OMCompiler/3rdParty/FMIL/ThirdParty/Expat/expat-2.0.1/lib"
  "OMCompiler/Compiler/runtime"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
