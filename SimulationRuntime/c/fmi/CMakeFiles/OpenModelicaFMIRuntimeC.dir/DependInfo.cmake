# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/fmi/FMI1CoSimulation.c" "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/fmi/CMakeFiles/OpenModelicaFMIRuntimeC.dir/FMI1CoSimulation.c.o"
  "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/fmi/FMI1Common.c" "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/fmi/CMakeFiles/OpenModelicaFMIRuntimeC.dir/FMI1Common.c.o"
  "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/fmi/FMI1ModelExchange.c" "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/fmi/CMakeFiles/OpenModelicaFMIRuntimeC.dir/FMI1ModelExchange.c.o"
  "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/fmi/FMI2Common.c" "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/fmi/CMakeFiles/OpenModelicaFMIRuntimeC.dir/FMI2Common.c.o"
  "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/fmi/FMI2ModelExchange.c" "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/fmi/CMakeFiles/OpenModelicaFMIRuntimeC.dir/FMI2ModelExchange.c.o"
  "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/fmi/FMICommon.c" "/home/lennart/ERC/OpenModelica/OMCompiler/SimulationRuntime/c/fmi/CMakeFiles/OpenModelicaFMIRuntimeC.dir/FMICommon.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/lib/expat-win32-msvc"
  "/include/lis"
  "/include/pthread"
  "OMCompiler"
  "OMCompiler/3rdParty/gc/include"
  "SimulationRuntime/c"
  "SimulationRuntime/c/linearization"
  "SimulationRuntime/c/math-support"
  "SimulationRuntime/c/meta"
  "SimulationRuntime/c/meta/gc"
  "SimulationRuntime/c/simulation"
  "SimulationRuntime/c/optimization"
  "SimulationRuntime/c/simulation/results"
  "SimulationRuntime/c/simulation/solver"
  "SimulationRuntime/c/simulation/solver/initialization"
  "SimulationRuntime/c/util"
  "OMCompiler/3rdParty/FMIL/install_msvc/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
